/*
1) Armar el hardware
2) Hacer andar un FreeRTOS
3) Definir una task en el RTOS que colecte la temperatura cada una cierta cantidad de tiempo
   configurable mediante un #Define
4) Definir una task en el RTOS que imprima lo que hay en el buffer de temperatura, cada una
   cierta cantidad de tiempo, configurable mediante un #define, y que sea más rápido que la velocidad a la que se generan los datos.
*/

/* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/* Xilinx includes. */
#include "xil_printf.h"
#include "xparameters.h"
/* XADC includes*/
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xsysmon.h"
#include "xparameters.h"
#include "sleep.h"


#define DELAY_10_SECONDS	10000UL
#define DELAY_1_SECOND		1000UL


/* The Tx and Rx tasks as described at the top of this file. */
static void prvThread1(void *pvParameter);
static void prvThread2(void *pvParameter);



/*-----------------------------------------------------------*/

static TaskHandle_t xThread1;
static TaskHandle_t xThread2;
static QueueHandle_t xQueue;

/*----------------------------------------------------------*/
#define SYSMON_DEVICE_ID  XPAR_SYSMON_0_DEVICE_ID
#define XSysMon_RawToExtVoltage(AdcData)  ((((float)(AdcData))*(1.0f))/65536.0f)

static XSysMon SysMonInst;
static int SysMonFractionToInt(float FloatNum);




int main( void )
{
	const TickType_t x1seconds = pdMS_TO_TICKS( DELAY_1_SECOND );
	const TickType_t x100ms = pdMS_TO_TICKS(100);

	xQueue = xQueueCreate( 4, sizeof( float ) );

	xil_printf( "Prueba FreeRTOS\r\n" );

	/* Create the five tasks. */

	xTaskCreate(prvThread1,
			    (const char *) "Lect",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY,
				&xThread1);

	xTaskCreate(prvThread2,
				(const char *) "Imp_print",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY,
				&xThread2);

	/* Start the tasks and timer running. */
	vTaskStartScheduler();

	/* If all is well, the scheduler will now be running, and the following line
	will never be reached.  If the following line does execute, then there was
	insufficient FreeRTOS heap memory available for the idle and/or timer tasks
	to be created.  See the memory management section on the FreeRTOS web site
	for more details. */
	for( ;; );
}

static void prvThread1( void *pvParameters)
{
	u32 TempRawData,VccIntRawData,ExtVolRawData;
	float TempData,VccIntData,ExtVolData;
	int xStatus;
	XSysMon_Config *SysMonConfigPtr;
	XSysMon *SysMonInstPtr =&SysMonInst;

	SysMonConfigPtr = XSysMon_LookupConfig(SYSMON_DEVICE_ID);
	if(SysMonConfigPtr == NULL) printf("Lookupconfig FAILURE\n\r");

	xStatus = XSysMon_CfgInitialize(SysMonInstPtr, SysMonConfigPtr, SysMonConfigPtr->BaseAddress);
	if(XST_SUCCESS !=xStatus) printf("CfgInitialize FAILURE\n\r");
	XSysMon_GetStatus(SysMonInstPtr);


  while(1){

	  TempRawData = XSysMon_GetAdcData(SysMonInstPtr,XSM_CH_TEMP);
	  TempData = XSysMon_RawToTemperature(TempRawData);

	  xQueueSend( xQueue, &TempData, portMAX_DELAY );

	  VccIntRawData = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_VCCINT);
	  VccIntData = XSysMon_RawToVoltage(VccIntRawData);

	  xQueueSend( xQueue, &VccIntData, portMAX_DELAY );

	  ExtVolRawData = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_AUX_MIN+1);
	  ExtVolData = XSysMon_RawToExtVoltage(ExtVolRawData);

	  xQueueSend( xQueue, &ExtVolData, portMAX_DELAY );

	  ExtVolRawData = XSysMon_GetAdcData(SysMonInstPtr, XSM_CH_AUX_MIN+9);
	  ExtVolData = XSysMon_RawToExtVoltage(ExtVolRawData);

	  xQueueSend( xQueue, &ExtVolData, portMAX_DELAY );

	  vTaskDelay(pdMS_TO_TICKS(DELAY_1_SECOND));


  }

}


static void prvThread2( void *pvParameters)
{
  float var;
  float curr;
  float pow;
  float val;

  while(1){
	  xQueueReceive( xQueue, &var, portMAX_DELAY );

	  xil_printf("\r\n La temperatura es %0d.%03d Celsius.\r\n",(int)(var), SysMonFractionToInt(var));

	  xQueueReceive( xQueue, &var, portMAX_DELAY );

	  xil_printf("El VCCint es %0d.%03d Volts. \r\n", (int)(var), SysMonFractionToInt(var));

	  xQueueReceive( xQueue, &var, portMAX_DELAY );

	  val=var*5.99;

	  xil_printf("La tensión de alimentación es %0d.%03d Volts. \r\n", (int)(val), SysMonFractionToInt(val));

	  xQueueReceive( xQueue, &var, portMAX_DELAY );

	  curr=var/0.25;

	  xil_printf("La corriente es %0d.%03d Amps. \r\n", (int)(curr), SysMonFractionToInt(curr));

	  pow=curr*val;

	  xil_printf("La potencia es %0d.%03d watts. \r\n", (int)(pow), SysMonFractionToInt(pow));

	  vTaskDelay(pdMS_TO_TICKS(DELAY_1_SECOND));
  }

}

int SysMonFractionToInt(float FloatNum)
{
	float Temp;
	Temp = FloatNum;
	if(FloatNum <0){
		Temp = -(FloatNum);

	}
	return ( ((int)((Temp -(float)((int)Temp)) *(1000))));
}

