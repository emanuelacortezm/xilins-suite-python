set part "xc7a35ticsg324-1L"
set board "digilentinc.com:arty-a7-35:part0:1.0"
set number_of_threads "4"

# Add here your own source code
#add_source "<SOURCE_CODE_NAME.v>"
#add_source "<SOURCE_CODE_NAME.v>"

# Add here your I/O and timing constraints
#add_constraint ./pinout.xdc
#add_constraint ./timing.xdc
