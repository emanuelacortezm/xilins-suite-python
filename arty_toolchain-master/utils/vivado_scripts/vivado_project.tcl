# Imputs from Args
set prj_dir  [lindex $argv 0]
set prj_name [lindex $argv 1]
set prj_task [lindex $argv 2]
set bd       [lindex $argv 3]
set syn      [lindex $argv 4]
set impl     [lindex $argv 5]
set bit      [lindex $argv 6]
set xsa      [lindex $argv 7]

proc add_source {s} {
    lappend ::settings(SRC) $s
}

proc add_constraint {s} {
    lappend ::settings(XDC) $s
}

proc add_constraint_tcl {s} {
    lappend ::settings(TCL) $s
}

proc set_settings {name value} {
    set ::settings($name) $value
}

proc get_settings {name} {
    return [set ::settings($name)]
}

# Defining sources
puts "Importing Settings"
source ${prj_dir}/src/settings/vivado.tcl -notrace

# Create Project
if {[string match -nocase "create_prj" $prj_task]} {
    create_project $prj_name ${prj_dir}/output/ -part $part
    set_property board_part $board [current_project]
    set_property target_language Verilog [current_project]
    if [info exist ::settings(SRC)] {
        puts "Importing Sources\n"
        set sources [get_settings SRC]
        foreach s $sources {
                puts "Adding file: $s"
                add_files -norecurse $s
        }
    }
    if [info exist ::settings(XDC)] {
        puts "Importing Constraints\n"
        set constraints [get_settings XDC]
        foreach c $constraints {
                puts "Adding constraint: $c"
                read_xdc $c
        }
    }

} else {
    open_project ${prj_dir}/output/${prj_name}.xpr
}

# Create Block Design
if {[string match -nocase "create_bd" $bd]} {
    create_bd_design ${prj_name}_bd
    puts "Importing User Block Design"
    source $prj_dir/src/settings/blockdesign.tcl -notrace
    puts "Creating Block Design"
    #! create_blockdesign
    generate_target all [get_files  ${prj_dir}/output/${prj_name}.srcs/sources_1/bd/${prj_name}_bd/${prj_name}_bd.bd]

    # Create HDL Wrapper
    puts "Creating HDL Wrapper"
    make_wrapper -files [get_files ${prj_dir}/output/${prj_name}.srcs/sources_1/bd/${prj_name}_bd/${prj_name}_bd.bd] -top
    add_files -norecurse ${prj_dir}/output/${prj_name}.srcs/sources_1/bd/${prj_name}_bd/hdl/${prj_name}_bd_wrapper.v

    set_property top ${prj_name}_bd_wrapper [current_fileset]
    update_compile_order -fileset sources_1
}

# Synthesis
if {[string match -nocase "do_synth" $syn]} {
    puts "Synthesizing Design"

    # Add constrains
    puts "Adding constrains"

    if [info exist ::settings(TCL)] {
        puts "Importing Constraints\n"
        set constraints_tcl [get_settings TCL]
        foreach c $constraints_tcl {
                puts "Adding constraint_tcl: $c"
                add_files -fileset constrs_1 $c
        }
    }

    puts "WARNING! This is not implemented yet"

    # Run implementation
    puts "Running Synthesis"
    launch_runs synth_1 -jobs $number_of_threads
    wait_on_run synth_1
}

# Implement Design
if {[string match -nocase "do_impl_dsgn" $impl]} {

    puts "Implementing Design"

    # Add constrains
    puts "Adding constrains"
    puts "WARNING! This is not implemented yet"

    launch_runs impl_1 -jobs $number_of_threads
    wait_on_run impl_1
}

# Bitstream
if {[string match -nocase "do_bitstream" $bit]} {
    puts "Generating Bitstream"

    launch_runs impl_1 -to_step write_bitstream -jobs $number_of_threads
    wait_on_run impl_1
}

# XSA
if {[string match -nocase "do_xsa" $xsa]} {
    puts "Generating XSA"

    write_hw_platform -fixed -include_bit -force -file  ${prj_dir}/output/hw/${prj_name}wrapper.xsa
}

exit
