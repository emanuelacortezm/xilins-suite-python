# Hardware Server Service

El `hw_server` es un programa provisto por Xilinx que debe ser ejecutado en la computadora a la cual se conecta el programador/debugger JTAG. Una vez que el mismo est� corriendo es posible acceder al programador/debugger tanto de forma local (en la misma computadora que corre el programa) o de forma remota. 

Para facilitar su uso se cre� un servicio de `systemd` el cual permite lanzarlo, detenerlo, relanzarlo o monitorear su estado de forma m�s sensilla. Este servicio se llama de igual manera `hw_server`. 

A continuaci�n se encuentran los pasos de instalaci�n del servicio y como utilizarlo. Vivado debe esar instalado previamente en la computadora a ser utililizada como servidor. 

# Instalaci�n y uso del servicio `hw_server`

## Instalaci�n

En el servidor al cual est� conectado el programador JTAG, realizar los siguientes pasos: 

1. Copiar el archivo `hw_server.service` de este repositorio a `/lib/systemd/system/` 

```
cd <remote_repo_dir>/utils/hw_server/
sudo cp hw_server.service /lib/systemd/system/
```

2. Recargar los demonios y habilitar el nuevo servicio

```
sudo systemctl daemon-reload 
sudo systemctl enable hw_server
```

3. Verificar estado del servicio

`sudo systemctl status hw_server`

## Modo de uso

1. Verificar estado del servicio

`sudo systemctl status hw_server`

2. Iniciar el servicio

`sudo systemctl start hw_server`

3. Detener el servicio

`sudo systemctl stop hw_server`

4. Re-iniciar el servicio

`sudo systemctl restart hw_server`

5. Ver el log del servicio

`sudo journalctl -u hw_server.service`
