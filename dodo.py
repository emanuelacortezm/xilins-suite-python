import os
from vivado import *
from vitis_doit import *
from programmer import *

TOOLCHAIN_PATH = os.getenv('TOOLCHAIN_PATH')

def clean_prj():
    os.system('rm output/ -rf')

def clean_bd():
    os.system('rm output/ -rf')

def task_prj():
    """Create Vivado Project"""
    print('Creating Vivado Project {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_SETTINGS],
        'targets' : [VIVADO_PROJECT_PATH+'/'+VIVADO_PROJECT_NAME+'.xpr'],
        'actions' : [PRJ_CMD],
        'clean'   : [clean_prj]
    }

def task_bd():
    """Create User Block Design """ 
    print('Creating Block Design for Project for {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [BLOCK_DESIGN],
        'targets' : [VIVADO_BD_PATH],
        'task_dep': ['prj'],
        'actions' : [BD_CMD],
        'clean'   : [clean_bd]
    }

def task_syn():
    """Design Synthesis"""
    print('Synthesis for Project {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_BD_PATH],
        'targets' : [VIVADO_SYNT],
        'task_dep': ['bd'],
        'actions' : [SYN_CMD],
        'clean'   : [clean_bd]
    }

def task_imp():
    """Design Synthesis"""
    print('Implementation for Project {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_SYNT],
        'targets' : [VIVADO_IMPL],
        'task_dep': ['syn'],
        'actions' : [IMP_CMD],
        'clean'   : [clean_bd]
    }

def task_bit():
    """Design Synthesis"""
    print('Bitstream for Project {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_IMPL],
        'targets' : [VIVADO_BIT],
        'task_dep': ['imp'],
        'actions' : [BIT_CMD],
        'clean'   : [clean_bd]
    }

def task_xsa():
    """ XSA Generation"""
    print('Hardware XSA for Project {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_BIT],
    #    'targets' : [VIVADO_BIT],
        'task_dep': ['bit'],
        'actions' : [XSA_CMD],
        'clean'   : [clean_bd]
    }



def task_prog_pl():
    """Program PL """
    print('Program PL for Project {}'.format(VIVADO_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_BIT],
        #'targets' : [VIVADO_BIT],
        'task_dep': ['bit'],
        'actions' : [PL_CMD],
        'clean'   : [clean_bd]
    }


def task_vitis_ws():
    """Create Vitis workspace"""
    print('Creating vitis workspace {}'.format(VITIS_PROJECT_NAME))
    return {
    #    'file_dep': [VIVADO_HW_XSA],
    #    'targets' : [VITIS_WS],
    #    'task_dep': ['xsa'],
        'actions' : [VITIS_WS],
        'clean'   : [clean_vitis]
    }

def task_vitis_plat():
    """Create Vitis platform"""
    print('Creating vitis platform {}'.format(VITIS_PROJECT_NAME))
    return {
        'file_dep': [VIVADO_HW_XSA],
    #    'targets' : [VITIS_WS],
        'task_dep': ['xsa'],
        'actions' : [VITIS_PLAT],
        'clean'   : [clean_vitis]
    }


def task_vitis_app():
    """Create Vitis application"""
    print('Creating vitis application {}'.format(VITIS_PROJECT_NAME))
    return {
        'file_dep': [PLATFORM],
    #    'targets' : [VITIS_PLAT],
        'task_dep': ['vitis_plat'],
        'actions' : [VITIS_APP],
        'clean'   : [clean_vitis]
    }

def task_vitis_imp():
    """Import source code Vitis"""
    print('Importing vitis sources {}'.format(VITIS_PROJECT_NAME))
    return {
    #    'file_dep': [VIVADO_SYSDEF],
    #    'targets' : [VITIS_APP],
        'task_dep': ['vitis_app'],
        'actions' : [VITIS_IMP],
        'clean'   : [clean_vitis]
    }


def task_vitis_build():
    """Build Vitis application"""
    print('Building vitis application {}'.format(VITIS_PROJECT_NAME))
    return {
    #    'file_dep': [VIVADO_SYSDEF],
    #    'targets' : [VITIS_IMP],
        'task_dep': ['vitis_imp'],
        'actions' : [VITIS_BUILD],
        'clean'   : [clean_vitis]
    }

def task_vitis_comp():
    """Program FPGA"""
    print('Programing FPGA {}'.format(VITIS_PROJECT_NAME))
    return {
    #    'file_dep': [SDK_PRJ],
    #    'targets' : [SDK_BUILD],
        'task_dep': ['vitis_build'],
        'actions' : [VITIS_COMP],
        'clean'   : [clean_vitis]
    }



def task_run_prog():
    """Run FPGA program"""
    print('Running FPGA {}'.format(VITIS_PROJECT_NAME))
    return {
        'file_dep': [WRAPPER, HW_V, ELF],
    #    'targets' : [SDK_BUILD],
        'task_dep': ['vitis_comp'],
        'actions' : [RUN_PROG],
        'clean'   : [clean_vitis]
    }